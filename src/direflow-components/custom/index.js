import { DireflowComponent } from 'direflow-component';
import App from './App';

export default DireflowComponent.create({
  component: App,
  configuration: {
    tagname: 'custom-component',
  },
  plugins: [
    {
      name: 'material-ui'
    },
  ],
});
