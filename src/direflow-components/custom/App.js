import React, { useState } from 'react';
import { Styled } from 'direflow-component';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import styles from './App.css'

const App = (props) => {
  const [task, setTask] = useState("");
  const [itemList, setItemList] = useState([
    {
      id : 0,
      value : "Create the to do App"
    },
    {
      id : 1,
      value : "Separate on different component"
    }
  ]);

  const handleKeyPress = (e) => {
    if(e.key === "Enter"){
      e.preventDefault();
      setTask("");

      // ajouter l'item dans la liste
      addItem(task);
    }
  }

  const addItem = (item) => {
    setItemList([...itemList, {
      id : itemList.length,
      value : item
    }]);    
  }

  const deleteItem = (id) => {
    const itemListNew = itemList.filter(item => {
      return item.id !== id;
    });
    setItemList(itemListNew);
  }

  const handleChange = (e) => {
    setTask(e.target.value);
  } 

  return (
    <>
      <Styled styles={styles}>
        <div>
          <form>
            <h1>A simple to do-list App </h1>
            <TextField className="champ" label="Ajouter votre tâche" onChange={handleChange} onKeyPress={handleKeyPress} value={ task } />
          </form> 

          <div className="liste">
            <List component="nav">
              { 
                itemList.length ? 
                itemList.map(item => (
                  <ListItem key={item.id} button onClick={ () => {deleteItem(item.id)}}>
                    <ListItemText primary={item.value} />
                  </ListItem>
                )) :
                <ListItem button>
                  <ListItemText primary="You don't have any task !! yay" />
                </ListItem>
              }
            </List> 
          </div>
        </div>   
      </Styled>
    </>
  );
};

App.defaultProps = {
  name: 'There',
}

App.propTypes = {
  name: PropTypes.string,
};

export default App;
